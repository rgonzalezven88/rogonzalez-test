const Joi = require('joi');
require('dotenv').config();

const envVarsSchema = Joi.object({
    NODE_ENV: Joi.string().allow(['development', 'test', 'production']).default('development'),
    PORT: Joi.number().default(8080),
    WEATHER_API: Joi.string().default('http://api.openweathermap.org/data/2.5/weather'),
    WEATHER_API_KEY: Joi.string().default('f61117fec2d3124a3929e9143e8a8860'),
    LOCATION_API: Joi.string().default('http://api.ipapi.com'),
    LOCATION_API_KEY: Joi.string().default('5c7b5b590939b163b3887ddc9354605c'),
}).unknown()
    .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}
const config = {
    env: envVars.NODE_ENV,
    port: envVars.PORT,
    weather_api: envVars.WEATHER_API,
    weather_api_key: envVars.WEATHER_API_KEY,
    location_api:  envVars.LOCATION_API,
    location_api_key:  envVars.LOCATION_API_KEY,
};

module.exports = config;