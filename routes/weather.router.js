'use strict';

const express = require('express');
const router = express.Router();
const Controller = require('../controllers/weather.controller');

/* GET get location info */
router.get('/location', Controller.getLocation);

/* GET get current weather */
router.get('/current/:city*?', Controller.getCurrent);

/* GET get forecast weather */
router.get('/forecast/:city*?', Controller.getForecast);

module.exports = router;