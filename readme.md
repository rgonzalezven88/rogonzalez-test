# Developer Applicant Interview Test

Este proyecto contiene el codigo solicito en la prueba tecnica enviada:

Se requiere implementar una API que provea en formato JSON el estado del tiempo basado en diferentes endpoints.
Se requiere realizar tests con las librerías antes mencionadas o con equivalentes.
A continuación se detallan los endpoints que deben ser implementados

## Iniciemos 🚀

### Ruta base
/v1
### Endpoints

* /location
Devuelve los datos de ubicación city según ip-api.
* /current[/city]
City es un parámetro opcional. Devuelve los datos de ubicación city o la ubicación actual según ip-api y el estado del tiempo actual.
* /forecast[/city]
City es un parámetro opcional. Devuelve los datos de ubicación city o la ubicación actual según ip-api y el estado del tiempo a 5 días

Mira **Despliegue** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Node / Npm_

### Instalación 🔧

_Instalar dependencias_

```
npm install
```

## Ejecutando las pruebas ⚙️

_Las pruebas fueron realizadas con mocha / chai / supertest_

```
npm run test
```

## Despliegue 📦

_Se utilizo la tecnologia dotenv para manejar las variables de entorno, y se creo un config que valida al iniciar estas configuraciones_

_Se añadio como adicional un build con webpack el cual permite concentrar en un solo file el codigo del back, esto mejoraria los tiempos de despliegue_

```
npm run build
```

_Para su despliegue en desarrollo se utilizo nodemon_

```
npm run dev
```

## Server de pruebas Heroku

_Para mejorar las pruebas se creo una instancia en Heroku con la app deployada en el endpoint: https://rogonzalez-test.herokuapp.com/v1_

* https://rogonzalez-test.herokuapp.com/v1/location
* https://rogonzalez-test.herokuapp.com/v1/current
* https://rogonzalez-test.herokuapp.com/v1/current/caracas
* https://rogonzalez-test.herokuapp.com/v1/forecast
* https://rogonzalez-test.herokuapp.com/v1/forecast/caracas


## Autor ✒️

* **Roberto Gonzalez** - *Developer* - [rogonzalez](https://github.com/rogonzalez88)