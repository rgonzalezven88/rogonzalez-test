'use strict';

const request = require('request');
const ResponseError = require('../helpers/ResponseError');

const config = require('../config/config');

const Service = {
  getLocation: (ip) => {
    const args = {
      method: 'GET',
      url: `${config.location_api}/${ip}`,
      qs: {
        access_key: config.location_api_key
      }
    };
    return new Promise((resolve, reject) => {
      request(args, (error, response, body) => {
        if (error) return reject(error);
        if (response.statusCode == 200) {
          try {
            resolve(JSON.parse(body));
          } catch (e) {
            reject(e);
          }
        } else {
          reject(new ResponseError(response.statusMessage, response.statusCode))
        }
      });
    });
  },
  getCurrent(city) {
    const args = {
      method: 'GET',
      url: `${config.weather_api}/weather`,
      qs: {
        q: city,
        appid: config.weather_api_key
      }
    };
    return new Promise((resolve, reject) => {
      request(args, (error, response, body) => {
        if (error) return reject(error);
        if (response.statusCode == 200) {
          try {
            resolve(JSON.parse(body));
          } catch (e) {
            reject(e);
          }
        } else {
          reject(new ResponseError(response.statusMessage, response.statusCode))
        }
      });
    });
  },
  getForecast(city) {
    const args = {
      method: 'GET',
      url: `${config.weather_api}/forecast`,
      qs: {
        q: city,
        appid: config.weather_api_key
      }
    };
    return new Promise((resolve, reject) => {
      request(args, (error, response, body) => {
        if (error) return reject(error);
        if (response.statusCode == 200) {
          try {
            resolve(JSON.parse(body));
          } catch (e) {
            reject(e);
          }
        } else {
          reject(new ResponseError(response.statusMessage, response.statusCode))
        }
      });
    });
  },
  async getCurrentByIp(ip) {
    const location = await this.getLocation(ip);
    const current = await this.getCurrent(location.city);
    return current;
  },
  async getForecastByIp(ip) {
    const location = await this.getLocation(ip);
    const current = await this.getForecast(location.city);
    return current;
  }
};

module.exports = Service;