var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var httpStatus = require('http-status');

var Router = require('./routes/weather.router');
var ResponseError = require('./helpers/ResponseError');
var config = require('./config/config');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors())

app.use('/v1', Router);

// Custom Errors
app.use((err, req, res, next) => {
  if (!(err instanceof ResponseError)) {
    const apiError = new ResponseError(err.message, err.status, err.isPublic);
    return next(apiError);
  }
  return next(err);
});

// 404 Error
app.use((req, res, next) => {
  const err = new ResponseError('Not found', 404);
  return next(err);
});

// Error Handler
app.use((err, req, res, next) =>
  res.status(err.status).json({
    message: err.isPublic ? err.message : httpStatus[err.status],
    stack: config.env === 'development' ? err.stack : {}
  })
);

module.exports = app;
