'use strict';

var app = require('../app'),
  chai = require('chai'),
  request = require('supertest');

var expect = chai.expect;

describe('Weather Api Tests', function () {
  it('should get location info', function (done) {
    request(app)
    .get('/v1/location')
    .set('x-forwarded-for', '34.34.132.50')
    .end(function (err, res) {
      expect(res.statusCode).to.equal(200);
      expect(res.body.city).to.equal('Houston');
      done();
    });
  });
  it('should get current weather by ip location', function (done) {
    request(app)
    .get('/v1/current')
    .set('x-forwarded-for', '34.34.132.50')
    .end(function (err, res) {
      expect(res.statusCode).to.equal(200);
      expect(res.body.name).to.equal('Houston');
      done();
    });
  });
  it('should get forecast weather by ip location', function (done) {
    request(app)
    .get('/v1/forecast')
    .set('x-forwarded-for', '34.34.132.50')
    .end(function (err, res) {
      expect(res.statusCode).to.equal(200);
      expect(res.body.city.name).to.equal('Houston');
      done();
    });
  });  
  it('should get current weather by city', function (done) {
    request(app)
    .get('/v1/current/caracas')
    .end(function (err, res) {
      expect(res.statusCode).to.equal(200);
      expect(res.body.name).to.equal('Caracas');
      done();
    });
  });
  it('should get forecast weather by city', function (done) {
    request(app)
    .get('/v1/forecast/caracas')
    .end(function (err, res) {
      expect(res.statusCode).to.equal(200);
      expect(res.body.city.name).to.equal('Caracas');
      done();
    });
  });
});
