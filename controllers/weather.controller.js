'use strict';

const Service = require('../services/weather.service');

let Controller = {
  getLocation: (req, res, next) => {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    Service.getLocation(ip)
      .then(result => res.json(result))
      .catch(e => next(e))
    ;
  },
  getCurrent: (req, res, next) => {
    const { city } = req.params;
    if (city) {
      Service.getCurrent(city)
        .then(result => res.json(result))
        .catch(e => next(e))
      ;
    } else {
      const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      Service.getCurrentByIp(ip)
        .then(result => res.json(result))
        .catch(e => next(e))
      ;
    }
  },
  getForecast: (req, res, next) => {
    const { city } = req.params;
    if (city) {
      Service.getForecast(city)
        .then(result => res.json(result))
        .catch(e => next(e))
      ;
    } else {
      const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      Service.getForecastByIp(ip)
        .then(result => res.json(result))
        .catch(e => next(e))
      ;
    }
  }
};

module.exports = Controller;